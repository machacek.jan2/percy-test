const httpServer = require('http-server');
const percySnapshot = require('@percy/webdriverio');

async function createTodo() {
  let newTodo = await $('.new-todo');
  newTodo.setValue('New fancy todo');
  await browser.keys('Enter');
  // needed or the todo is never submitted
  await browser.execute(() => document.querySelector('.new-todo').blur());
}

describe('example page', function () {
  const PORT = 8000;
  const TEST_URL = `http://localhost:${PORT}`;

  let server = null;

  before(() => {
    // Start local server to host app under test.
    server = httpServer.createServer({ root: `${__dirname}/../` });
    server.listen(PORT);
  });

  after(() => {
    // Shut down the HTTP server.
    server.close();
  });

  it('Loads the app', async function () {
    await browser.url(TEST_URL);
    const title = await browser.getTitle();
    expect(title).toEqual('Todo list');
    await percySnapshot(browser, 'Homepage Snapshot');
});

it('Accepts a new todo', async function () {
    await browser.$('.new-todo').setValue('New fancy todo');
    await browser.keys('Enter');
    await browser.pause(500);  // Adding a short pause to ensure the DOM updates

    const todos = await browser.$$('.todo-list li');
    expect(todos.length).toEqual(1);
    await percySnapshot(browser, 'Snapshot with new todo');
});

it('Lets you check off a todo', async function () {
  await browser.$('.new-todo').setValue('Complete me');
  await browser.keys('Enter');
  await browser.pause(500);  // Ensure the todo has been added

  const completeButton = await browser.$('.toggle');
  await completeButton.click();
  await browser.pause(500);  // Wait for the application to update

  const itemsLeftText = await browser.$('.todo-count').getText();
  expect(itemsLeftText.trim()).toEqual('1 item left');  // Using trim() to remove any extra spaces
  await percySnapshot(browser, 'Snapshot with completed todo');
});
});
