# example-percy-webdriverio

Example app showing integration of [Percy](https://percy.io/) visual testing into WebDriverIO tests.


## WebDriverIO Tutorial

### Step 1

Clone the example application and install dependencies:


The example app and its tests will now be ready to go. You can explore the app
by opening the
[`index.html`](https://github.com/percy/example-percy-webdriverio/blob/master/index.html)
file in a browser.

### Step 2

Sign in to Percy and create a new project. You can name the project "todo" if you'd like. After
you've created the project, you'll be shown a token environment variable.

### Step 3

In the shell window you're working in, export the token environment variable:

**Unix**

``` shell
$ export PERCY_TOKEN="<your token here>"
```

**Windows**

``` shell
$ set PERCY_TOKEN="<your token here>"

# PowerShell
$ $Env:PERCY_TOKEN="<your token here>"
```

Note: Usually this would only be set up in your CI environment, but to keep things simple we'll
configure it in your shell so that Percy is enabled in your local environment.

### Step 4

Check out a new branch for your work in this tutorial (we'll call this branch
`tutorial-example`), then run tests & take snapshots:

``` shell
$ npm install
$ npm run test
```

This will run the app's WebDriverIO tests, which contain calls to create Percy snapshots. The snapshots
will then be uploaded to Percy for comparison. Percy will use the Percy token you used in **Step 2**
to know which organization and project to upload the snapshots to.

You can view the screenshots in Percy now if you want, but there will be no visual comparisons
yet. You'll see that Percy shows you that these snapshots come from your `tutorial-example` branch.

### Step 5

Use your text editor to edit `index.html` and introduce some visual changes. For example, you can
add inline CSS to bold the "Clear completed" button on line 32. After the change, that line looks
like this:



### Useful links 
https://qxf2.com/blog/percy-challenges-and-grey-areas/
