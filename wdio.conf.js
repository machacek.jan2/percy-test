exports.config = {
  runner: 'local',
  framework: 'mocha',
  reporters: ['spec'],
  specs: ['./specs/*.test.js'],
  logLevel: 'debug',  // Zvýšená úroveň logování pro diagnostiku
  services: [['chromedriver', { port: 9515 }]], // Specifikování portu pro ChromeDriver
  capabilities: [
    {
      maxInstances: 5,
      browserName: 'chrome',
      acceptInsecureCerts: true,
      'goog:chromeOptions': {
        args: ['--headless', '--disable-gpu', '--window-size=1920,1080']
      }
    }
  ],

  // onPrepare a onComplete funkce mohou být odebrány, pokud nejsou potřeba

  onPrepare() {
    require('chromedriver').start();
  },

  onComplete() {
    require('chromedriver').stop();
  }
};
